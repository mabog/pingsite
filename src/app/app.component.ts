import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    constructor(
    private http: HttpClient,
  ) { }

  private SERVER_ADDR: string = "http://localhost:3000";
  private $subscription: Subscription;

  public _pingTimes: string;
  public _speedResults: string;
  public pingForm: FormGroup = new FormGroup({
    ip: new FormControl('', [Validators.required, Validators.minLength(1)]),
    count: new FormControl('',),
  });

  public ping(target: string, count:number): void {
    this.$subscription = this.http.post<[]>(`${this.SERVER_ADDR}/ping`, {target, count}).subscribe(data => {
      // console.log(data)////
      let str: string = data.toString();
      let re = /\,/g;
      str = str.replace(re, "ms ") + 'ms'
      this._pingTimes = str
    })
  }

  public test(): void {
    this._speedResults = "Running test... ETA: 1min"
    this.$subscription = this.http.post<string>(`${this.SERVER_ADDR}/speedtest`, {}).subscribe(data => {
      this._speedResults = 'Down ' + data + 'Up'
    })
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.$subscription.unsubscribe()
  }
}
